# SRC

Short for `SOURCE`, I think ...

Anyway, usually this is the place usually used to put your scripts, functions and classes definitions that you don't want to copy and paste in many places, but use in many places. It's basically a collection of helpers for the notebooks in `notebooks`.

For example, the most common one would be:

- `utils.py`: usually you can put most **util[itie]s** functions/classes in here
- you can also put them in more contained files designated to their purpose or stage as well, like:
  - `preprocess.py` for only preprocessing purposes
  - `analysis.py` for actual analysis scripts and pipelines
  - `visualization.py` for more plotting utility functions


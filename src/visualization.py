from matplotlib import pyplot as plt 

def plot_data(df, title='opinion'):
    norm_response = 100 * df['response']
    norm_response = norm_response / sum(norm_response)
    
    plt.xkcd(randomness=5)
    
    plt.barh(
        df['opinion'], 
        norm_response,
        lw = 2,
        edgecolor = 'k',
        facecolor = [0.8,0.9,0.9]
    )
    plt.gca().invert_yaxis()
    plt.title(title)
    plt.xlabel('% people')
    plt.show()
# An ephemeral analysis of *stuff*

This is an ephemeral repository for analyzing stuff.

## Data

- Data constitutes of stuff. 
- Then stuff births out more data.

## Analysis

- One has to ponder, what analysis can be done with **stuff**? 
- And philosophically, isn't everything *ephemeral*?
- Isn't stuff itself is ephemeral?

Scientists are obsessed about **stuff** and sometimes consider themselves as the `stuff` to study. They ask: 

> Isn't it amazing? We're stuff studying stuff itself.

Then they ponder, like any pondering ponderers: 

> The stuff gets very stuffy in here, on this earth yet not very stuff elsewhere in the universe.

Are astronomers studying the *non stuffy stuff* because they hate feeling stuffy?



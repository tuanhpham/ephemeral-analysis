# Config

Usually this can be a place for your to store certain program configurations, like plotting or certain analysis configs. For example, sometimes you have a set of 10 different parameters for your models or certain analysis. And you want certain fonts, certain font sizes, certain line widths, ... You can save such "meta-data" here, especially the plotting configs can then be loaded and used across many notebooks. But more improtantly, if your analysis has more than, say, 10 hyperparameters, it would help you and the people looking at your repo if it is in a `yaml` or `toml` file. 


# Docs

Usually this is a place for:

- Documentations
- Manuscripts, or at the official versions (like for publications)
- Posters at times
- Sometimes can also be a place for logos, general non-data media
- Other guidelines, ...


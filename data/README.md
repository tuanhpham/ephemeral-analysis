# Data

This is a data folder, usually people tend to `.gitignore` the actual files in here because data binary files are usually very big. But we tend to keep the data structure. So we create the `.gitkeep` files.
